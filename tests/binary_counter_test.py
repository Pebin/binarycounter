__author__ = 'petr'

import unittest
from binary_counter import BinaryCounter


class TestBinaryCounter(unittest.TestCase):
    def setUp(self):
        self.test_to_number = 1337
        self.binary_counter = BinaryCounter()

    def test_binary_counter(self):
        i = 0
        for i in xrange(self.test_to_number):
            self.binary_counter.increase()
            if int(str(self.binary_counter), 2) != i + 1:
                break

        self.assertEqual(int(str(self.binary_counter), 2), i + 1)

    def test_not_increased_binary_counter(self):
        expected_result = "0"
        counted_result = str(self.binary_counter)

        self.assertEqual(expected_result, counted_result)

    def test_zero_after_increase_binary_counter(self):
        expected_result = "0"
        self.binary_counter.increase()
        self.binary_counter.zero()

        self.assertEqual(expected_result, str(self.binary_counter))


if __name__ == '__main__':
    unittest.main()