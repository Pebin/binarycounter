__author__ = 'petr'


class BinaryCounter:
    def __init__(self):
        # binary content should contain small lists representing pairs like [3, 0], which is then interpreted as three
        # zeros in row
        self.binary_content = []

    def increase(self):
        # Case when content is empty
        if len(self.binary_content) == 0:
            self.binary_content.append([1, 1])
        else:
            # Find first position where is 0 and all 1 before this position set to zero
            if self.binary_content[0][1] == 1:
                actual_index = 1
                self.binary_content[0][1] = 0
            else:
                actual_index = 0

            # In case there are no more items, add 1
            if len(self.binary_content) - actual_index <= 0:
                self.binary_content.append([1, 1])
            # There is one item with 0, so there must be next item with [x, 1] where x is > 0
            elif self.binary_content[actual_index][0] == 1:
                self.binary_content[actual_index + 1][0] += 1
                self.binary_content.pop(actual_index)
            # Last possible case is, that there is more than one zero on actual position
            elif self.binary_content[actual_index][0] > 1:
                self.binary_content[actual_index][0] -= 1
                self.binary_content.insert(actual_index, [1, 1])
            else:
                raise Exception("HAHA: LOOKS LIKE THIS DOESN'T WORK")

    def zero(self):
        self.binary_content = []

    def __str__(self):
        binary_representation = ""
        for pair in self.binary_content:
            binary_representation = ''.join(str(pair[1]) for _ in range(pair[0])) + binary_representation

        if binary_representation == "":
            binary_representation = "0"

        return binary_representation